
pattern : C42

Some parameters of AB1CB2 tetrablock terpolymers are given below.
 Nx : the number of discretization node of spatial variable x.
 Ny : the number of discretization node of spatial variable y.
 ds : the step size of the contour s.
The c2c file represents the Fourier coefficients are given the size of Nx*Ny.
 
The r2c file represents the Fourier coefficients are given the size of Nx*(Ny/2+1).

The pha.dat is the initial value of real space, the first column is the density of A, the second column is the density of B1, the third column is the density of C, the fourth column is the density of B2.

    dim : 2
 Nx Ny  : 64 64
     ds : 0.001
  chiBC : 0.80
  chiAC : 0.80
  chiAB : 0.80
     fA : 0.14 
    fB1 : 0.27 
     fC : 0.12
    fB2 : 1-fA-fB1-fC
 domain : 4.1 0.0 0.0 4.1
 lx ly  : 1  1
