
clc
clear
N =64;
Nx=N;
Ny=N;
  phi=load('pha.dat');
  phiA=reshape(phi(:,1),[N,N]);
  phiB=reshape(phi(:,2)+phi(:,4),[N,N]);
  phiC=reshape(phi(:,3),[N,N]);

 phiA = ifft2(phiA);
 phiB = ifft2(phiB);
 phiC = ifft2(phiC);
for i=1:Nx
    if (i>Nx/2)
            k1=i-Nx-1;
    else
        k1=i-1;
    end
     for j=1:Ny
        if (j>Ny/2)
             k2=j-Ny-1;
         else
             k2=j-1;
        end
            K{i,j}=[k1,k2];
     end
end

  realvalA=real(phiA);
  imagvalA=imag(phiA);

 realvalB=real(phiB);
  imagvalB=imag(phiB);
   realvalC=real(phiC);
  imagvalC=imag(phiC);

%数据保存到文件

 fidA = fopen('C42_phiA_c2c.txt', 'wt');
for i=1:Ny
    for j=1:Ny
 fprintf(fidA, '%g\t', K{i,j}(1));
 fprintf(fidA, '%g\t', K{i,j}(2));
fprintf(fidA, '%g\t',realvalA(i,j));
 fprintf(fidA, '%g\t', imagvalA(i,j));
fprintf(fidA, '\n');
    end
end
fclose(fidA);

fidB = fopen('C42_phiB_c2c.txt', 'wt');

for i =1:Nx
    for j=1:Ny
fprintf(fidB, '%g\t', K{i,j}(1));
fprintf(fidB, '%g\t',K{i,j}(2));
fprintf(fidB, '%g\t', realvalB(i,j));
fprintf(fidB, '%g\t', imagvalB(i,j));
fprintf(fidB, '\n');
    end
end
fclose(fidB);

fidC = fopen('C42_phiC_cToc.txt', 'wt');
for i =1:Nx
    for j=1:Ny
fprintf(fidC, '%g\t', K{i,j}(1));
fprintf(fidC, '%g\t', K{i,j}(2));
fprintf(fidC, '%g\t', realvalC(i,j));
fprintf(fidC, '%g\t', imagvalC(i,j));
fprintf(fidC, '\n');
    end
end
fclose(fidC);
