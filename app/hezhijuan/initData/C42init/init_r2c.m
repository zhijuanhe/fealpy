% N = realDofs;
N = 64;
count=N*(N/2+1);
phi=load('pha.dat');
phiA=reshape(phi(:,1),[N,N]);
phiB=reshape(phi(:,2)+phi(:,4),[N,N]);
phiC=reshape(phi(:,3),[N,N]);


phiA = ifft2(phiA);
phiA = phiA(:,1:N/2+1);%取其中的一半
phiB = ifft2(phiB);
phiB = phiB(:,1:N/2+1);
phiC = ifft2(phiC);
phiC = phiC(:,1:N/2+1);

K1 = ones(N,1);
K2 = ones(1,N/2+1);
y = 0:N/2;
x = [0:N/2 -N/2+1:-1];

Y = kron(K1,y);
X = kron(K2,x');
[valA,indexA] = sort(phiA(:),'descend');
realvalA=real(valA);
imagvalA=imag(valA);
XindexA = X(indexA);
YindexA = Y(indexA);

[valB,indexB] = sort(phiB(:),'descend');
realvalB=real(valB);
imagvalB=imag(valB);
XindexB = X(indexB);
YindexB = Y(indexB);

[valC,indexC] = sort(phiC(:),'descend');
realvalC=real(valC);
imagvalC=imag(valC);
XindexC = X(indexC);
YindexC = Y(indexC);

%数据保存到文件
fidA = fopen('C42_phiA_r2c.txt', 'wt');
% fprintf(fidA, '%g\n', count);
for i =1:N*(N/2+1)
fprintf(fidA, '%g\t', XindexA(i));
fprintf(fidA, '%g\t', YindexA(i));
fprintf(fidA, '%g\t', realvalA(i));
fprintf(fidA, '%g\t', imagvalA(i));
fprintf(fidA, '\n');
end
fclose(fidA);

fidB = fopen('C42_phiB_r2c.txt', 'wt');
% fprintf(fidB, '%g\n', count);
for i =1:N*(N/2+1)
fprintf(fidB, '%g\t', XindexB(i));
fprintf(fidB, '%g\t', YindexB(i));
fprintf(fidB, '%g\t', realvalB(i));
fprintf(fidB, '%g\t', imagvalB(i));
fprintf(fidB, '\n');
end
fclose(fidB);

fidC = fopen('C42_phiC_r2c.txt', 'wt');
% fprintf(fidC, '%g\n', count);
for i =1:N*(N/2+1)
fprintf(fidC, '%g\t', XindexC(i));
fprintf(fidC, '%g\t', YindexC(i));
fprintf(fidC, '%g\t', realvalC(i));
fprintf(fidC, '%g\t', imagvalC(i));
fprintf(fidC, '\n');
end
fclose(fidC);
